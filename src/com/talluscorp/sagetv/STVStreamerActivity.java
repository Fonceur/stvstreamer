package com.talluscorp.sagetv;

import java.util.UUID;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class STVStreamerActivity extends MainActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.main);
        initialize();
    }
    
    private String getID(){
    	String ID = fileId.getText().toString();
		
		if (!ID.equalsIgnoreCase(id)){
			id = ID;
			final SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			final SharedPreferences.Editor editor = settings.edit();
			editor.putString("FileId", id);
			editor.commit();
		}
    	
    	return id;
    }
    
	public String getFileURL(String ID)	{
		if (ID == null || ID.length() < 1 || ID.equalsIgnoreCase("-1"))
			return "http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8";
		
	    final StringBuilder sbURL = new StringBuilder("http");
	    sbURL.append("://").append(hostname);
	    sbURL.append(":").append(jettyPort);
	    sbURL.append("/sagex/media/mediafile?mediafile=").append(ID);
	    
	    Log.i(tag, "URL = " + sbURL);
		return sbURL.toString();
	}

	public String getStreamingURL(boolean is23, String ID)	{
		if (ID == null || ID.length() < 1 || ID.equalsIgnoreCase("-1"))
			return "http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8";
		
	    final StringBuilder sbURL = new StringBuilder((is23 ? "httplive" : "http"));
	    sbURL.append("://").append(hostname);
	    sbURL.append(":").append(jettyPort);
	    sbURL.append("/stream/HTTPLiveStreamingPlaylist?MediaFileId=").append(ID);
	    
	    if (rate > 0){
	    	sbURL.append("&ConversionId=");
	    	sbURL.append(UUID.randomUUID());
	    	sbURL.append("&Quality=");
	    	
	    	switch (rate){
		    	case 1: sbURL.append(150); break;
		    	case 2: sbURL.append(1240); break;
		    	case 3: sbURL.append(1840); break;
		    	case 4: sbURL.append(840); break;
		    	case 5: sbURL.append(640); break;
		    	case 6: sbURL.append(440); break;
		    	case 7: sbURL.append(240); break;
	    	}
	    }
	    //sbURL.append("&ConversionId=e2e7dc52-51ef-415e-a929-45fa52a78048&Quality=150&MediaFileSegment=0");
	    
	    Log.i(tag, "URL = " + sbURL);
		return sbURL.toString();
	}
    
    private void initialize(){
		Log.i(tag, "Starting " + getVersionName(this));
		final SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

		id = settings.getString("FileId", "61773");
		hostname = settings.getString("Hostname", "192.168.0.100");
		jettyPort = settings.getString("JettyPort", "8080");
		rate = settings.getInt("BitRate", 0);
		
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.my_title);

		final ImageButton configBtn  = (ImageButton) this.findViewById(R.id.config);
		configBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showSettings();
			}
		});
		
		final Button mirageBtn  = (Button) this.findViewById(R.id.btnMirageStream);
		mirageBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mirageStream();
			}
		});
		final Button mxBtn  = (Button) this.findViewById(R.id.btnMXStream);
		mxBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mxStream(false);
			}
		});
		final Button mxProBtn  = (Button) this.findViewById(R.id.btnMXProStream);
		mxProBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mxStream(true);
			}
		});
		final Button androidBtn  = (Button) this.findViewById(R.id.btnHTTPLiveStream);
		androidBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				androidStream(true);
			}
		});
		final Button androidBtn3  = (Button) this.findViewById(R.id.btnHTTPStream);
		androidBtn3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				androidStream(false);
			}
		});

		final Button mirageBtnPlay  = (Button) this.findViewById(R.id.btnMiragePlay);
		mirageBtnPlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				miragePlay();
			}
		});
		final Button mxBtnPlay  = (Button) this.findViewById(R.id.btnMXPlay);
		mxBtnPlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mxPlay(false);
			}
		});
		final Button mxProBtnPlay  = (Button) this.findViewById(R.id.btnMXProPlay);
		mxProBtnPlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mxPlay(true);
			}
		});
		final Button androidBtn3Play  = (Button) this.findViewById(R.id.btnHTTPPlay);
		androidBtn3Play.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				androidPlay();
			}
		});

		fileId = (EditText) this.findViewById(R.id.fileId);
		fileId.setText(id);
    }
    
	public void androidPlay() {
		try{
			//Thread.sleep(streamingDelay * 500);
	    	final Intent playingIntent = new Intent("android.intent.action.VIEW");
	    	playingIntent.setDataAndType(Uri.parse(getFileURL(getID())), "video/*");
	    	startActivity(playingIntent);
		} catch (Exception ex) {}
	}

	public void androidStream(boolean is23) {
		try{
			//Thread.sleep(streamingDelay * 500);
	    	final Intent streamingIntent = new Intent("android.intent.action.VIEW", 
	    			Uri.parse(getStreamingURL(is23, getID())));
	    	startActivity(streamingIntent);
		} catch (Exception ex) {}
	}

    private void miragePlay(){
		Intent i = new Intent(Intent.ACTION_VIEW); 

	    i.setClassName("com.bw.xplayer.ui", "com.bw.xplayer.ui.PlayView");  
	    i.setData(Uri.parse(getFileURL(getID())));
	    i.putExtra("name", "STVStreamer"); // explicit name/label instead of derived from url
	    i.putExtra("persistent", false); // persist to history
	    try{
	    	startActivity(i);
	    }
	    catch (Exception ex){Log.i(tag, ex.getMessage());}
    }
    
    private void mirageStream(){
		Intent i = new Intent(Intent.ACTION_VIEW); 

	    i.setClassName("com.bw.xplayer.ui", "com.bw.xplayer.ui.PlayView");  
	    i.setData(Uri.parse(getStreamingURL(false, getID())));
	    i.putExtra("name", "STVStreamer"); // explicit name/label instead of derived from url
	    i.putExtra("persistent", false); // persist to history
	    try{
	    	startActivity(i);
	    }
	    catch (Exception ex){Log.i(tag, ex.getMessage());}
    }

	public void mxPlay(boolean isPro) {
		Intent i = new Intent(Intent.ACTION_VIEW); 

		if (isPro){
			i.setClassName("com.mxtech.videoplayer.pro", "com.mxtech.videoplayer.pro.ActivityScreen");  
			i.setData(Uri.parse(getFileURL(getID())));
		}
		else{
			i.setClassName("com.mxtech.videoplayer.ad", "com.mxtech.videoplayer.ad.ActivityScreen");
		    i.setData(Uri.parse(getFileURL(getID())));
		}
	    try{
	    	startActivity(i);
	    }
	    catch (Exception ex){Log.i(tag, ex.getMessage());}
	}

	public void mxStream(boolean isPro) {
		Intent i = new Intent(Intent.ACTION_VIEW); 

		if (isPro){
			i.setClassName("com.mxtech.videoplayer.pro", "com.mxtech.videoplayer.pro.ActivityScreen");  
			i.setData(Uri.parse(getStreamingURL(false, getID())));
		}
		else{
			i.setClassName("com.mxtech.videoplayer.ad", "com.mxtech.videoplayer.ad.ActivityScreen");
		    i.setData(Uri.parse(getStreamingURL(false, getID())));
		}
	    try{
	    	startActivity(i);
	    }
	    catch (Exception ex){Log.i(tag, ex.getMessage());}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
			case KeyEvent.KEYCODE_BACK:
				final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
				alertDialog.setMessage("Do you really want to quit?");
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
				      public void onClick(DialogInterface dialog, int which) {
				    	  finish();
				    	  return;
				    } });
			    alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {
			      public void onClick(DialogInterface dialog, int which) {
			        return;
			    }}); 
				alertDialog.show();

				return true;

			default:
				return super.onKeyDown(keyCode, event);
		}
	} 
}