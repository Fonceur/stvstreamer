package com.talluscorp.sagetv;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends Activity{
	public static EditText fileId;
	public static int rate = 0;
	public static String id = "61773";
	public static String hostname = "192.168.0.100";
	public static String jettyPort = "8080";

	public static final String PREFS_NAME = "SageTVStreamerPrefs";
	public static final String tag = "MainActivity";
	
	public static String getVersionName(Context context){
		final StringBuilder sb = new StringBuilder("STVStreamer");
	  
		try {
			final ComponentName comp = new ComponentName(context, "STVStreamer");
			final PackageInfo pinfo = context.getPackageManager()
				.getPackageInfo(comp.getPackageName(), 0);
			sb.append(" version ");
			sb.append(pinfo.versionName);
		} catch (android.content.pm.PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		
		return sb.toString();
	}

	public void showSettings() {
		final LayoutInflater inflater = 
			(LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.serverconfig,
				(ViewGroup) this.findViewById(R.id.serverview));
		final EditText editHostname = (EditText) layout.findViewById(R.id.hostname);
		final EditText editPort = (EditText) layout.findViewById(R.id.jettyPort);
		final Spinner s = (Spinner) layout.findViewById(R.id.rateSpinner);
		editHostname.setText(hostname);
		editPort.setText(jettyPort);
		final String[] rates = {"Variable", "150 kBps", "1240 kBps", "1840 kBps", "840 kBps", "640 kBps", "440 kBps", "240 kBps"};
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String> (
				this, android.R.layout.simple_spinner_item, rates);
 		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    s.setAdapter(adapter);
	    s.setSelection(rate);

		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setView(layout);
		builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				hostname = editHostname.getText().toString();
				jettyPort = editPort.getText().toString();
				rate = s.getSelectedItemPosition();
				final SharedPreferences settings = getSharedPreferences(MainActivity.PREFS_NAME, 0);
				final SharedPreferences.Editor editor = settings.edit();
				editor.putString("Hostname", hostname);
				editor.putString("JettyPort", jettyPort);
				editor.putInt("BitRate", s.getSelectedItemPosition());

				// Don't forget to commit your edits!!!
				editor.commit();
		        return;
		    }
		});
		builder.setNegativeButton(android.R.string.cancel, null);
		final AlertDialog alertDialog = builder.create();
		alertDialog.show();
		alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	}
}
